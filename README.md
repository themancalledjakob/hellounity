# plain helloUnity example

Here an a short introduction video, explaining you what you could do with this.

![Explanatory Video](watchMe.mp4)

If you can download unity (https://unity.com), and open this project, you are set for next class.

Should you want to, why not play around with images and how to add them to Unity?
If you run into any issues with that, just ask.
